#include <stdio.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include <gsl/gsl_linalg.h>

void matrix_print(char s[], gsl_matrix* M)
{
	printf("%s\n",s);
	for(int i=0; i< M->size1; i++)
		{
		for(int j=0; j<M->size2; j++){
			printf("%10g ",gsl_matrix_get(M,i,j));
			}
		printf("\n");
		}
	printf("\n\n");
}

void vector_print(char s[], gsl_vector* v)
	{
	printf("%s\n",s);
	for(int i=0;i<v->size;i++)printf("%10g ",gsl_vector_get(v,i));
	printf("\n\n");
	}


int main (void){
	//Matrixdimensions definet as n
	double n=3;

	double A_data[3][3] = { 
		{6.13, -2.90, 5.86},
		{8.08, -6.31, -3.89},
		{-4.36, 1.00, 0.19}};

	double b_data[3] = {6.23, 5.37, 2.29};

	gsl_matrix* A=gsl_matrix_alloc(n,n);
	gsl_matrix* A_data2=gsl_matrix_alloc(n,n);
	gsl_vector* x=gsl_vector_alloc(n);
	gsl_vector* b=gsl_vector_alloc(n);
	gsl_vector* b_calc=gsl_vector_alloc(n);

	for(int i=0; i< b->size; i++)
		{
		gsl_vector_set(b,i,b_data[i]);
		}
		
	for(int i=0; i< A->size1; i++)
		{
		for(int j=0; j<A->size2; j++)
			{
			gsl_matrix_set(A,i,j,A_data[i][j]);
			}
		}
	gsl_matrix_memcpy(A_data2,A);
	gsl_linalg_HH_solve(A,b,x);

	
	matrix_print("Matrix A is",A_data2);
	vector_print("Vector b is",b);
	vector_print("Vector x is",x);
	
	//Prove it is the right x
	gsl_blas_dgemv(CblasNoTrans,1.0,A_data2,x,0.0,b_calc);
	vector_print("A*x should be equal to b=",b_calc);

	gsl_matrix_free(A);
	gsl_matrix_free(A_data2);
	gsl_vector_free(x);
	gsl_vector_free(b);
	gsl_vector_free(b_calc);
return 0;
}
