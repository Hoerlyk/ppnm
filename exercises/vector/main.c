#include "vector.h"
#include "stdio.h"
#include "assert.h"
#include "stdlib.h"
#define RND (double)rand()/RAND_MAX

int main()
{
	int n = 5;

	printf("\nmain: testing vector_alloc ...\n");
	vector *v = vector_alloc(n);
	if (v == NULL) printf("test failed\n");
	else printf("test passed\n");

	printf("\nmain: testing vector_set and vector_get ...\n");
	double value = RND;
	int i = n / 2;
	vector_set(v, i, value);
	double vi = vector_get(v, i);
	if (vi==value) printf("test passed\n");
	else printf("test failed\n");

	printf("\nmain: testing vector_add ...\n");
	vector *a = vector_alloc(n);
	vector *b = vector_alloc(n);
//	vector *c = vector_alloc(n);	
	for (int i = 0; i < n; i++) {
//		double x = RND, y = RND;
		vector_set(a, i, 0);
		vector_set(b, i, 0);
//		vector_set(c, i, x + y);
	}

	vector_set(a,1,1);
	vector_set(b,2,1);

	value=vector_dot_product(a,b);

//	vector_add(a, b);
	vector_print("a dot b should   = 0 \n");
	vector_print("a dot b actually = %g \n\n", value);
	
	value=vector_dot_product(a,a);
	printf("a dot a should   = 1\n");
	printf("a dot a actually = %g \n\n", value);

//	if (vector_equal(c, a)) printf("test passed\n");
//	else printf("test failed\n");

	vector_free(v);
	vector_free(a);
	vector_free(b);
//	vector_free(c);
	return 0;
}
