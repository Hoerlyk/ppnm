#ifndef HAVE_VECTOR_H /* for multiple includes */
#define HAVE_VECTOR_H

typedef struct {int size; double* data;} vector;

vector* vector_alloc       (int n);     /* allocates memory for size-n vector */
void    vector_free        (vector* v);                      /* frees memory */
void    vector_set         (vector* v, int i, double value); /* v_i ← value */
double  vector_get         (vector* v, int i);              /* returns v_i */
double  vector_dot_product (vector* u, vector* v);   /* returns dot-product */

/*// optional 
void vector_print    (char* s, vector* v);    // prints s and then vector 
void vector_set_zero (vector* v);             // all elements ← 0 
int  vector_equal    (vector* a, vector* b); // 1, if equal, 0 otherwise 
void vector_add      (vector* a, vector* b); // a_i ← a_i + b_i 
void vector_sub      (vector* a, vector* b); // a_i ← a_i - b_i 
void vector_scale    (vector* a, double x);   // a_i ← x*a_i     
*/

#endif
