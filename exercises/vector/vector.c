#include<stdio.h>
#include <stdlib.h>
#include<assert.h>
#include"vector.h"

vector* vector_alloc(int n)
{
	vector* v = malloc(sizeof(vector));
	(*v).size = n;
	(*v).data = malloc(n*sizeof(double));
	if( v==NULL ) fprintf(stderr,"error in vector_alloc\n");
	return v;
}

void vector_free(vector* v){ free(v->data); free(v);} 


void vector_set(vector* v, int i, double value)
{
	assert( 0 <= i && i < (*v).size );
	(*v).data[i]=value;
}


double vector_get(vector* v, int i)
{
	assert( 0 <= i && i < (*v).size );
	return (*v).data[i];
}


double vector_dot_product(vector* u, vector* v)
{	
	double dp=0;
	for(int i=0; i< u->size; i++) dp+=(*u).data[i]*(*v).data[i];
	return dp;
}



