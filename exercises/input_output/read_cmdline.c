#include<stdio.h>
#include<stdlib.h>
#include<math.h>
int main(int argc, char** argv){
	if(argc<2) fprintf(stderr,"%s: there were no arguments\n",argv[0]);
	else {
		for(int i=1;i<argc;i++){
			double x = atof(argv[i]);
			printf("argument number %i: x= %g sin(x)=%g cos(x)=%g \n",i,x,sin(x),cos(x));
		}
	}
	printf("\n");
return 0;
}
