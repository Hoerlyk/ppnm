#include<stdio.h>
#include<math.h>

int main(){
	double x;
	int items;
	FILE* my_in_stream = fopen("inputfile.txt","r");
	FILE* my_out_stream = fopen("outputfile.txt","w");

	do{
		items = fscanf(my_in_stream,"%lg",&x);	//reads from my_in_stream
		if(items==EOF)break;			//continues til end of file
		fprintf(my_out_stream,"x=%lg sin(x)=%lg cos(x)=%lg \n",x,sin(x),cos(x));	//print values to my_out_stream
	}while(1);

	fclose(my_in_stream);
	fclose(my_out_stream);
	
return 0;
}
