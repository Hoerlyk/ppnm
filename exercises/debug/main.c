#include"stdio.h"
#include"gsl/gsl_matrix.h"	//must be gsl/gsl_matrix.h instead of just gsl_matrix.h

int print_half_00(gsl_matrix* m)	//must be gsl_matrix not gsl_matrix*
{
	double half = 1./2.;
	int status = printf( "half m_{00} = %f\n", gsl_matrix_get(m,0,0)*half );  //must be %f instad of %i
	//gsl_matrix_free(m);		this is already done in the main
	return status;
}

int main(void)
{
	gsl_matrix* m = gsl_matrix_alloc(1,1);		//must be gsl_matrix* not gsl_matrix // you can not make  matrix of only 0 rows and 0 column
	gsl_matrix_set(m,0,0,66);			//not &m - only m
	printf("half m_{00} (should be 33):\n");
	int status = print_half_00(m);			//not &m - only m
	if(status==0)					//printf returns zero if it doesn't do what it has to do and return the number of caracters it prints if it works. Therefore if status=0 something went wrong
		printf("status=%d : SOMETHING WENT TERRIBLY WRONG (status=0)\n",status);	//must be %d instead of %g
	else
		printf("status=%d : everything went just fine (status not equal to 0)\n",status);		//must be %d instead of %g
	gsl_matrix_free(m);				//should be m not &m
return 0;
}
