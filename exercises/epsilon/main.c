#include<stdio.h>
#include<limits.h>
#include<float.h>

int tau();

int main()
{
	//Generally i,j,k,l,m is while loop
	//Generally c,d,e,f,g is for loop
	//Generally u,v,x,y,z is do loop
	//Generally w,t is the float sum
	//Generally p,q is the double sum
	printf("1.i)\n");
	
	//integer max
	int i=INT_MAX-1000; while(i+1>i) {i++;}
	printf("my max int using while = %i\n",i);

	int c; for(c=INT_MAX-1000; c+1>c; c++){} 
	printf("my max int using for = %i\n",c);

	int u=INT_MAX-1000;
	do{
	u++;
	}while(u+1>u);
	printf("my max int using do = %i\n",u);

	printf("Using INT_MAX = %i \n\n",INT_MAX);



	printf("1.ii)\n");
	
	//integer min
	int j=INT_MIN+1000; while(j-1<j) {j--;}
	printf("my min int using while = %i\n",j);
		
	int d; for(d=INT_MIN+1000; d-1<d; d--){}
	printf("my min int using for = %i\n",d);

	int v=INT_MIN+1000;
	do{
	v--;
	}while(v-1<v);
	printf("my min int using do = %i\n",v);

	printf("Using INT_MIN = %i\n\n",INT_MIN);
	


	printf("1.iii)\n");
	
	//double epsilon
	double k=1; 
	while(1+k!=1){k/=2;} k*=2;
	printf("Double epsilon with while = %g\n",k);

	double e; for(e=1; 1+e!=1; e/=2){} e*=2;
	printf("Double epsilon with for = %g\n",e);

	double x=1;	
	do{
	x/=2;
	}while(1+x!=1); x*=2;
	printf("Double epsilon with do = %g\n",x);

	printf("DBL_EPSILON = %g\n\n",DBL_EPSILON);

	//float epsilon
	float l=1;
	while(1+l!=1){l/=2;} l*=2;
	printf("Float epsilon with while = %g\n",l);

	float f; 
	for(f=1; 1+f!=1; f/=2){} f*=2;
	printf("Float epsilon with for = %g\n",f);

	float y=1;
	do{
	y/=2;
	}while(1+y!=1); y*=2;
	printf("Float epsilon with do = %g\n",y);

	printf("FLT_EPSILON = %g\n\n",FLT_EPSILON);

	//long double
	long double m=1;
	while(1+m!=1){m/=2;} m*=2;
	printf("Long double epsilon with while = %Lg\n",m);

	long double g;
	for(g=1; 1+g!=1; g/=2){} g*=2;
	printf("Long double epsilon with for = %Lg\n",g);

	long double z=1;
	do{
	z/=2;
	}while(1+z!=1); z*=2;
	printf("Long double epsilon with do = %Lg\n",z);
	
	printf("LDBL_EPSILON = %Lg\n\n",LDBL_EPSILON);

	printf("In all cases the found value is equal to what the command from limits.h finds.\n\n\n");


	printf("2.i)\n");
	//float_sum
	int max=(int)1e7;

	float w=1;
	float sum=0;
	while(w<max){sum+=1.0f/w;w++;}
	printf("Harmonic sum from 1 up to %i in float =%g\n",max,sum);
	//float sum_up_float = 1.0f + 1.0f/2 + 1.0f/3 + ... + 1.0f/max;

	int t=max;
	float sum2=0;
	while(t>0){sum2+=1.0f/t;t--;}
	printf("Harmonic sum from %i down to 1 in float =%g\n\n",max,sum2);


	printf("2.ii)\n");
	printf("It can be seen that there are differences in the sums already at the second decimal this is because that the correct way to add numbers is to ad numbers of approximately the same order. Since the then the loss of accuracy is the least possible. Therefore the harmonic sum from the max to 1 gives the correct result. \n\n");
	
	printf("2.iii)\n");
	printf("A Harmonic series like the ones made in 2.ii) diverges logarithmically. If a series diverges logarithmically it diverges very slowly and therefore it is not a problem here as long as the sum is made in the correct way. That is such that small numbers is added to small numbers to ensure a minimal loss of accuracy. \n\n");

	printf("2.iv)\n");
	//double sum
	double q=1;
	double sum3=0;
	while(q<max){sum3+=1.0/q;q++;}
	printf("Harmonic sum from 1 to %i in double =%g\n",max,sum3);
	
	double p=max;
	double sum4=0;
	while(p>0){sum4+=1.0/p;p--;}
	printf("Harmonic sum from %i down to 1 in double =%g\n",max,sum4);

	printf("It can now be seen that the sums are the same no matter of summing up or down. This is because that the smalles number that can possibly be added is much smaller for a double (approximately 2e-16) than for a float (approximately 1e-7). Therefore the loss of accuracy is smaller when doing the sum with doubles than with floats. \n\n");

	tau();

return 0;
}
