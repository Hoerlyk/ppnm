#include<math.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<gsl/gsl_vector.h>
#define RND ((double)rand()/RAND_MAX-0.5)*2 //Defines better and smaller random


#define FMT "%7.3f"
void print_matrix(gsl_matrix* A) //The matrix function (like it was made in the exercise about gsl-matrices) but only using the first five rows and columns of the matrix since the one used here is pretty large. 	
{
	for (int i=0;i<5;i++) 	//Small function which sets very small values (below 10^-6) to zero in order to make the matrices nicer and actually diagonal when looking at it
	{
		for(int j=0;j<5;j++)
		{
			if(fabs(gsl_matrix_get(A,i,j))<1e-6)gsl_matrix_set(A,i,j,0);
		}
	}

	for(int n=0;n<5;n++)
	{
		for(int m=0;m<5;m++) printf("%20g",gsl_matrix_get(A,n,m));
		printf("\n");
	}
}

//The vector print function is made available for later use - it is made like it was in the gsl-matrix lecture
void vector_print(gsl_vector* v){
	for(int i=0;i<v->size;i++) printf("%12g ",gsl_vector_get(v,i));
		printf("\n");
}

//Functions from GS.c
void GS_decomp(gsl_matrix* A, gsl_matrix* R); //Decomposes QR
void QR_back  (gsl_matrix* A, gsl_vector* R); //Performs back substitution
void GS_solve(gsl_matrix* Q, gsl_matrix* R, gsl_vector* b,gsl_vector* qb); 
void GS_inverse(gsl_matrix* Q, gsl_matrix* R, gsl_matrix *B);

void jacobi_diag(gsl_matrix* H, gsl_matrix* V);

void arnoldi_iteration(gsl_matrix* A, gsl_matrix* Q, gsl_vector* q1,int n);
//void jacobi2(gsl_matrix* A, gsl_matrix* V);


int main(int argc, char** argv)
{		
	//Integers for use through out the exercise
	size_t n=6,m=6; //unsinged int
	
	printf("\n\nLanczos tridiagonalization algorithm\n\n");


	printf("For this project a random vector is needed to start the Arnoldi iteration. This vector is generated randomly by c.\n");
	
	gsl_vector* q1 = gsl_vector_alloc(n);

	for(int i=0;i<n;i++)
	{
		gsl_vector_set(q1,i,RND);
	}

	printf("\nThe random vector q1: \n");
	gsl_vector_fprintf(stdout,q1,FMT);



	//Generate random matrix A
	gsl_matrix *A = gsl_matrix_calloc(m,n);
	gsl_matrix *R = gsl_matrix_calloc(m,m);
	for(int i=0;i<n;i++) 
	{
		for(int j=0;j<m;j++) 
		{
			double number=RND;
			gsl_matrix_set(A,i,j,number);
			gsl_matrix_set(A,j,i,number);
		}
	}

	//Print the random matrix A
	printf("A random generated real symmetric random matrix A:\n"); print_matrix(A);


	gsl_matrix* Q = gsl_matrix_calloc(m,n);

	gsl_matrix_set_zero(Q);

	//The arnoldi iteration is implemented in order to create the vector Q
//	arnoldi_iteration(A,Q,q1,n);


	printf("\nNow the Arnoldi iteration is implemented and it builds the matrix Q which can be used to make a partial orthogonal reduction of A into a matrix H in the form of a Hessenberg matrix. \n");
	print_matrix(Q);

	printf("My arnoldi iteration doesn't work so therefore i just use the hamiltonian matrix from the eigenvalues exercise. \n\n ");



	//Finding eigenvectors and eigenvalues of H	
	
	printf("\n\nQR-diagonalization can be used to find the eigenvectors and eigenvalues of the Hessenberg matrix H.\n");
	printf("Here a modified Gram-Schmidt orthogonalization of the matrix A is performed in order to check that it works. This is equivalent to the linear equations exercise.  \n\n");
	
	//Factorization of A into QR
	GS_decomp(A,R);
	
	//Print matrix Q and R
	printf("Matrix Q:\n"); print_matrix(A);
	printf("Matrix R (should be upper triangular):\n"); print_matrix(R);


	
	//Calculate Q^TQ which should be equal to the identity matrix (1 on the diagonal) and print it
	gsl_matrix *qtq = gsl_matrix_calloc(m,m);
	gsl_blas_dgemm(CblasTrans,CblasNoTrans,1.0,A,A,0.0,qtq);
	printf("Matrix Q^TQ (should be the identity matrix):\n"); print_matrix(qtq);
	
	//Calculate and print QR to check that it is equal to A
	gsl_matrix *qr = gsl_matrix_calloc(n,m);
	gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,A,R,0.0,qr);
	printf("Matrix QR (should be equal A):\n"); print_matrix(qr);



	printf("\n\n Now a Hermitian matrix is build in order to check the routines for finding eigenvalues and eigenvectors since the building of the Hermitian matrix doesn't work. The hermitian matrix build is close to the one made in the eigenvalues homework. \n\n ");

//	size_t n=6; 
	double s=1.0/(n+1);
	
	//The matrix H and V is allocated
	gsl_matrix* T =gsl_matrix_calloc(n,n);
	gsl_matrix* V =gsl_matrix_alloc(n,n);
	
	//V is made the identity matrix
	gsl_matrix_set_identity(V);
	
	//Now the Hermitian matrix is build (like in the exercise on the homepage)	
	for(int i=0;i<n-1;i++)
	{
		gsl_matrix_set(T,i,i,2);
		gsl_matrix_set(T,i,i+1,1);
		gsl_matrix_set(T,i+1,i,1);
	}
	gsl_matrix_set(T,n-1,n-1,-2);
	gsl_matrix_scale(T,1/s/s);

	
	//the matrices X, Y and HH are allocated
	gsl_matrix* HH=gsl_matrix_alloc(n,n);
	gsl_matrix* X =gsl_matrix_alloc(n,n);
	gsl_matrix* Y =gsl_matrix_alloc(n,n);

	//HH is made a copy of H
	gsl_matrix_memcpy(HH,T);

	//Now Matrix H is printed an it is checked that it is just like the one in the exercise
	printf("Matrix T (Only the first 5 rows and columns since the rest is equal to this and then it just takes up too much space):\n");
	print_matrix(T);


/*
	//Now new matrix X is made as X=V^T*H
	gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1,V,T,0,X);
	//Here matrix Y is made as Y=X*V which is the same as Y=V^T*H*V
	gsl_blas_dgemm(CblasNoTrans,CblasTrans,1,X,V,0,Y);
	//The matrix Y is printed and should equal the digonal jacobi matrix D
	printf("Matrix V*T*V^T - should be equal to A (first 5 rows and columns):\n");
	print_matrix(Y);
*/

/*
	//Now new matrix X is made as X=V*H
	gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1,A,T,0,X);
	//Here matrix Y is made as Y=X*V^T which is the same as Y=V^T*H*V
	gsl_blas_dgemm(CblasNoTrans,CblasTrans,1,X,A,0,Y);
	//The matrix Y is printed and should equal the digonal jacobi matrix D
	printf("Matrix V*T*V^T - should be equal to A (first 5 rows and columns):\n");
	print_matrix(Y);
*/


	printf("\n\n");


	//Now H is diagonalized 
	jacobi_diag(T,V);
		
	//And then the diagonalized H is printed (it can also be called D)
	printf("matrix H after jacobi - from now on called D (first 5 rows and columns):\n");
	print_matrix(T);
	
	//Now new matrix X is made as X=V^T*H
	gsl_blas_dgemm(CblasTrans,CblasNoTrans,1,V,HH,0,X);
	//Here matrix Y is made as Y=X*V which is the same as Y=V^T*H*V
	gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1,X,V,0,Y);
	//The matrix Y is printed and should equal the digonal jacobi matrix D
	printf("matrix V^T*H*V - should be equal to D (first 5 rows and columns):\n");
	print_matrix(Y);

	//Now matrix X is made as X=V*D
	gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1,V,T,0,X);
	//And matrix Y is Y=X*V^T which equals Y=V*D*V^T
	gsl_blas_dgemm(CblasNoTrans,CblasTrans,1,X,V,0,Y);
	//The matrix Y is printed and should equal the original matrix H
	printf("matrix V*D*V^T - should be equal to H (first 5 rows and columns):\n");
	print_matrix(Y);


	//Now the matrix X is calculated as V*V^T 
	gsl_blas_dgemm(CblasTrans,CblasNoTrans,1,V,V,0,X);
	//The matrix X is printed and should be equal to the identity matrix (1)
	printf("matrix V^T*V - should be the identity matrix (1) (first 5 rows and columns):\n");
	print_matrix(X);






return 0;
}





