#include <stdio.h> 
#include <math.h> 
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_vector.h>


/*

//The first three functions is needed for the Gram-Schmidt orthogonalization on the vectors in Q. 
//It comes from the webpage https://www.quora.com/What-is-the-C-code-for-Gram-Schmidt-orthogonalization-on-a-set-of-vectors-in-R4

double dot_product(double *x, double *y,int n) { 
	 
	int i; 
	double ans = 0; 
	
	for(i=0; i<n; ++i) 
		ans += x[i]*y[i]; 
	
	return ans; 
} 
 
void normalize(double *x, int n) { 
	 
	// Compute norm  
	double norm = sqrt(dot_product(x, x)); 
	
	int i; 
	for(i=0; i<n; ++i) 
		x[i] /= norm; 
} 
 
// Find an orthonormal basis for the set of vectors q using the Gram-Schmidt Orthogonalization process 
void gram_schmidt(q[][n], int m, int n) { 
	int i, j, k; 

	for(i=1; i<m; ++i) { 
		for(j=0; j<i; ++j) { 
			double scaling_factor = dot_product(q[j], q[i])		
				/ dot_product(qj, qj); 
			
			// Subtract each scaled component of q_j from q_i 
			for(k=0; k<n; ++k) 
				q[i][k] -= scaling_factor*q[j][k]; 
		} 
	} 
	
	// Now normalize all the 'n' orthogonal vectors 
	for(i=0; i<n; ++i) 
		normalize(q[i]); 
} 

*/

//Made with inspiration from the algorithm at http://www.math.iit.edu/~fass/477577_Chapter_14.pdf 
//i have tried to implement it with gsl_vectors

void arnoldi_iteration(gsl_matrix* A, gsl_matrix* Q, gsl_vector* q1, int n)
{
//	gsl_vector* q1 = gsl_vector_alloc(n);
	gsl_vector* v  = gsl_vector_alloc(n);
//	gsl_vector* q_ny=gsl_vector_alloc(n);
//	gsl_vector* q  = gsl_vector_alloc(n);
//	gsl_vector* q_save=gsl_vector_alloc(n);
	double hjn;
	int q1_norm;
	int h_ny;

	q1_norm=1/gsl_blas_dnrm2(q1);
	gsl_vector_scale(q1,q1_norm);	//Normalization of b so it is now called q1 from blas
	gsl_matrix_set_col(Q,0,q1);	//

	for(int k=1;k<=n;k++)
	{
		gsl_vector_view q = gsl_matrix_column(Q,k);  //Split the matrix up in the current column (found in GNU-libary)
		gsl_blas_dgemv(CblasNoTrans,1,A,&q.vector,0,v);	//Finding v=Aq_n 

		for(int j=1; j<=n;j++)
		{	
//			double* hjn;
			gsl_blas_ddot(&q.vector,v,&hjn);	//scalarproduct of vectors q*T and v returning hjn but as a pointer
			double h_access = hjn;
			gsl_vector_add_constant(&q.vector,h_access);	//From normal gsl_vectors - addition of a constant/scalar
			gsl_vector_sub(v,&q.vector);
		}
		h_ny=gsl_blas_dnrm2(v);
		gsl_vector_scale(v,1/h_ny);
		gsl_matrix_set_col(Q,k,v);
	}


}








