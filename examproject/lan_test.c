



//This function is made with inspiration from the wikipedia example for python
void arnoldi_iteration(gsl_matrix* A,gsl_vector* b,int n)
{
/*Computes a basis of the (n + 1)-Krylov subspace of A: the space spanned by {b, Ab, ..., A^n b}.

	Arguments
	A: m × m array
	b: initial vector (length m)
	n: dimension of Krylov subspace, must be >= 1
					          
	Returns
	Q: m x (n + 1) array, the columns are an orthonormal basis of the
	Krylov subspace.
	h: (n + 1) x n array, A on basis Q. It is upper Hessenberg.  
*/
	int m = int n

	gsl_matrix* h = gsl_matrix_calloc(n+1,n);
//	gsl_matrix* Q = gsl_matrix_alloc(m,n+1);

	gsl_vector* q_1 = gsl_vector_alloc(b);
//	gsl_vector* v = gsl_vector_alloc(n);	

//	h = np.zeros((n + 1, n))
//	Q = np.zeros((m, n + 1))
//	q = b / np.linalg.norm(b)  // Normalize the input vector
//	Q[:, 0] = q  // Use it as the first Krylov vector

	double q=b/gsl_blas_dnrm2(b);

//	Q(:,0)=q;

	for(int k=1;k<n;k++)
	{
		gsl_vector* v = gsl_blas_ddot(A,q);
		//v = A.dot(q)
//		v = gsl_blas_ddot(A,q)  //Generate a new candidate vector
		
		
		for (int j,j<(k+1);j++)
		{  // Subtract the projections on previous vectors

			hjn= gsl_blas_ddot(q,v);
			gsl_matrix_set(h,q,j,hjn);
			//h[j, k] = gsl_blas_ddot(Q(:,j).conj(), v)
			v=v-hjn*q;
		
			//v = v - h[j, k] * Q[:, j]
		}

		hn1n=gsl_blas_dnrm2(v);
//		h[k + 1, k] = np.linalg.norm(v)
		eps = 1e-12  // If v is shorter than this threshold it is the zero vector

		q=v/hn1n;
	
/*		if h(k + 1, k) > eps:  // Add the produced vector to the list, unless
			q = v / h[k + 1, k]  // the zero vector is produced.
			Q[:, k + 1] = q
		else:  //If that happens, 
			break; //stop iterating.
*/	return q, h;
	}
gsl_matrix_free(Q);

return Q, h;
}
