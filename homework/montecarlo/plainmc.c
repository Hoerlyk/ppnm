#include <math.h>
#include <stdlib.h>
#include <complex.h>
#include <assert.h>
#define RND ((double)rand( )/RAND_MAX)
//void randomx (int dim , double *a , double *b , double *x )
//{ for ( int i =0; i<dim ; i++) x[i]=a[i]+RND*(b[i]−a[i]); }

//Complex plain monte carlo from exercise
double complex plainmc(int dim,double f(int dim,double* x),double* a,double* b,int N){
        double V=1; for(int i=0;i<dim;i++)V*=b[i]-a[i];
        double sum=0,sum2=0,x[dim];
        for(int i=0;i<N;i++){
                for(int i=0;i<dim;i++)x[i]=a[i]+RND*(b[i]-a[i]);
                double fx=f(dim,x); 
		sum+=fx; 
		sum2+=fx*fx;
                }
        double mean=sum/N, sigma=sqrt(sum2/N-mean*mean);
        complex result=mean*V+I*sigma*V/sqrt(N);
        return result;
}



//void plainmc (int dim , double *a , double *b ,
//double f (double* x ) , int N, double* result , double* error)
//{ double V=1; for (int i =0; i<dim ; i++) V*=b[i]−a[i];
//	double sum=0, sum2=0, fx, x[dim];
//		for ( int i=0; i<N; i++){ randomx (dim,a,b,x); fx=f(x);
//		sum+=fx ; sum2+=fx * fx ;}
//	double avr = sum/N, var = sum2/N−avr * avr ;
//	*result = avr*V; *error = sqrt (var/N)*V;
//}
