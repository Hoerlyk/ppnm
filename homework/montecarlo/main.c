#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<complex.h>
#include <assert.h>

double complex plainmc(int dim,double f(int dim,double* x),double* a,double* b,int N); // The function for the monte carlo simulation which returns two values (the real and the imaginary part of the result)

complex quasimc(int dim,double f(int dim,double*x),double*a,double*b,int N); //Will be used for part b

#define R 0.9
double f(int dim,double* p){
	assert(dim==2);
	double x=p[0],y=p[1];
	if(x*x+y*y<R*R)return 1;
	else return 0;
	}

	double f1(int dim, double* p)
	{
		double x=p[0], y=p[1], z=p[2];
	
		return 1/(M_PI*M_PI*M_PI*(1-cos(x)*cos(y)*cos(z)));
	}




int main(int argc,char** argv)
{
	double a[]={0,0},b[]={1,1};
	int dim=sizeof(a)/sizeof(a[0]);
	
	if(argc>1)
	{
		srand(42);
		int N=(int)atof(argv[1]);	
		complex result_p=plainmc(dim,f,a,b,N);
		complex result_q=quasimc(dim,f,a,b,N);
		double integ_p=creal(result_p);
		double integ_q=creal(result_q);
		double exact=M_PI/4*R*R;
		double error_p=fabs(integ_p-exact);
		double error_q=fabs(integ_q-exact);
		printf("%i %g %g\n",N,error_p,error_q);
	}
	else {
		int N=(int)1e4;
		complex result_p=plainmc(dim,f,a,b,N);
		complex result_q=quasimc(dim,f,a,b,N);
		double integ_p=creal(result_p), esterr_p=cimag(result_p);
		double integ_q=creal(result_q), esterr_q=cimag(result_q);
		double exact=M_PI/4*R*R;
		double error_p=fabs(integ_p-exact);
		double error_q=fabs(integ_q-exact);
		printf("Integrating x*x+y*y<%g*%g?1:0 with N=%i\n",R,R,N);
		printf("Pseudo-random:\n");
		printf("Integral = %f error estimate = %f\n",integ_p,esterr_p);
		printf("Exact    = %f actual error   = %f\n",exact,error_p);
		printf("Quasi-random:\n");
		printf("Integral = %f error estimate = %f\n",integ_q,esterr_q);
		printf("Exact    = %f actual error   = %f\n",exact,error_q);

		dim=3;
		double a1[]={0,0,0},b1[]={M_PI,M_PI,M_PI};	
/*		double f1(int dim, double* p)
		{
			double x=p[0], y=p[1], z=p[2];
			
			return 1/(M_PI*M_PI*M_PI*(1-cos(x)*cos(y)*cos(z)));
		}
*/
	
		result_p=plainmc(dim,f1,a1,b1,N);
		integ_p=creal(result_p), esterr_p=cimag(result_p);	
		double exact1=1.3932039296856768591842462603255;
		error_p=fabs(integ_p-exact1);
		printf("integrating  1/(PI^3*(1-cos(x)*cos(y)*cos(z))) with N=%i\n",N);
		printf("pseudo-random:\n");
		printf("integral = %f error estimate = %f\n",integ_p,esterr_p);
		printf("exact    = %f actual error   = %f\n",exact1,error_p);
		
		printf("_______________________ \nExercise B\n");
		printf("integrating x*x+y*y<%g*%g with N=%i\n",R,R,N);
		printf("quasi-random:\n");
		printf("integral = %f error estimate = %f\n",integ_q,esterr_q);
		printf("exact    = %f actual error   = %f\n\n",exact,error_q);

		printf("\nIt can be seen that the quasi Monte Carlo gives errors which is much smaller than the plain monte carlo. This is also shown in the plot out.error.png. ");
}

return 0;
}

