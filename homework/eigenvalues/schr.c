#include<math.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#define RND ((double)rand()/RAND_MHX-0.5)*2

#define FMT "%7.3f"
void printm(gsl_matrix* A) //The matrix function (like it was made in the exercise about gsl-matrices) but only using the first five rows and columns of the matrix since the one used here is pretty large. 	
{
	for (int i=0;i<5;i++) 	//Small function which sets very small values (below 10^-6) to zero in order to make the matrices nicer and actually diagonal when looking at it
		{
		for(int j=0;j<5;j++)
			{
			if(fabs(gsl_matrix_get(A,i,j))<1e-6)gsl_matrix_set(A,i,j,0);
			}
		}

	for(int n=0;n<5;n++)
		{
		for(int m=0;m<5;m++) printf("%20g",gsl_matrix_get(A,n,m));
		printf("\n");
		}
}

int jacobi_diag(gsl_matrix* H,gsl_matrix* V); //The jacobi function is made available

int main(int argc, char** argv)
{
	printf("\n\nPart B\n\n");

	size_t n=20; 
	double s=1.0/(n+1);
	//if(argc>1)n=atoi(argv[1]);
	
	//The matrix H and V is allocated
	gsl_matrix* H =gsl_matrix_calloc(n,n);
	gsl_matrix* V =gsl_matrix_alloc(n,n);
	
	//V is made the identity matrix
	gsl_matrix_set_identity(V);
	
	//Now the Hamiltonian matrix is build (like in the exercise on the homepage)	
	for(int i=0;i<n-1;i++)
		{
		gsl_matrix_set(H,i,i,-2);
		gsl_matrix_set(H,i,i+1,1);
		gsl_matrix_set(H,i+1,i,1);
		}
	gsl_matrix_set(H,n-1,n-1,-2);
	gsl_matrix_scale(H,-1/s/s);

	//the matrices X, Y and HH are allocated like in part A
	gsl_matrix* HH=gsl_matrix_alloc(n,n);
	gsl_matrix* X =gsl_matrix_alloc(n,n);
	gsl_matrix* Y =gsl_matrix_alloc(n,n);

	//HH is made a copy of H
	gsl_matrix_memcpy(HH,H);

	//Now Matrix H is printed an it is checked that it is just like the one in the exercise
	printf("Matrix H (Only the first 5 rows and columns since the rest is equal to this and then it just takes up too much space):\n");
	printm(H);
	
	//Now H is diagonalized like in part A
	jacobi_diag(H,V);
	
	//And then the diagonalized H is printed (it can also be called D)
	printf("matrix H after jacobi - from now on called D (first 5 rows and columns):\n");
	printm(H);
	
	//Now new matrix X is made as X=V^T*H
	gsl_blas_dgemm(CblasTrans,CblasNoTrans,1,V,HH,0,X);
	//Here matrix Y is made as Y=X*V which is the same as Y=V^T*H*V
	gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1,X,V,0,Y);
	//The matrix Y is printed and should equal the digonal jacobi matrix D
	printf("matrix V^T*H*V - should be equal to D (first 5 rows and columns):\n");
	printm(Y);

	//Now matrix X is made as X=V*D
	gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1,V,H,0,X);
	//And matrix Y is Y=X*V^T which equals Y=V*D*V^T
	gsl_blas_dgemm(CblasNoTrans,CblasTrans,1,X,V,0,Y);
	//The matrix Y is printed and should equal the original matrix H
	printf("matrix V*D*V^T - should be equal to H (first 5 rows and columns):\n");
	printm(Y);

	//Now the matrix X is calculated as V*V^T 
	gsl_blas_dgemm(CblasTrans,CblasNoTrans,1,V,V,0,X);
	//The matrix X is printed and should be equal to the identity matrix (1)
	printf("matrix V^T*V - should be the identity matrix (1) (first 5 rows and columns):\n");
	printm(X);

	//Now it is checked if the energies are correct by using the code from the exercise on the homepage
	printf("\nThe energies are printed. Here column 2 should be equal to column 3\n");
	for (int k=0; k < n/3; k++)
		{
		double exact = M_PI*M_PI*(k+1)*(k+1);
		double calculated = gsl_matrix_get(H,k,k);
		printf("%i %g %g\n",k,calculated,exact);
		}
	printf("\nIt seems that the numbers in column 2 is close to those in column 3 (but the difference becomes larger and larger as the number of eigenfunction increases). \n");

	//Now the function which calculaates the several lowest eigenfunctions (here the 3 lowest) is implemented (the code is equal to that in the exercise). These values is used to make the plot. Two points are added such that it is zero both at x = 0 and x = 1
	printf("\n\n#index 1: First 3 eigenfunctions: \n 0 0 0 0 \n");
	for(int i=0;i<n;i++) 
		printf("%g %g %g %g\n",(i+1.0)/(n+1), gsl_matrix_get(V,i,0), gsl_matrix_get(V,i,1), gsl_matrix_get(V,i,2));		;
	printf("1 0 0 0\n");  	

}

