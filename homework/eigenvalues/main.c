#include<math.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#define RND ((double)rand()/RAND_MAX-0.5)*2

#define FMT "%7.3f"
void print_matrix(gsl_matrix *A)	//The matrix print function (like made in the exercise about gsl-matrices
	{
	for(int r=0;r<A->size1;r++)
		{
		for(int c=0;c<A->size2;c++) printf(FMT,gsl_matrix_get(A,r,c));
		printf("\n");
		}
	}	

int jacobi_diag(gsl_matrix*A,gsl_matrix*V);	//The jacobi function is made available
//int jacobi2(gsl_matrix*A,gsl_vector*e,gsl_matrix*V);

int main(int argc, char** argv)
	{
	printf("\nPart A\n\n");
	
	size_t n=4;	//The dimension of the future comming matrices

	if(argc>1)n=atoi(argv[1]);	//Memory for matrix A and V is allocated
	gsl_matrix* A =gsl_matrix_alloc(n,n);
	gsl_matrix* V =gsl_matrix_alloc(n,n);
	//gsl_vector* evec=gsl_vector_alloc(n);
	
	gsl_matrix_set_identity(V);	//V is made the identity matrix of dimensions (n,n)
	for(int i=0;i<n;i++)
		for(int j=i;j<n;j++) //A is created as a random matrix with entrances a_{ij} (also random)
			{
			double aij=RND;
			gsl_matrix_set(A,i,j,aij);
			gsl_matrix_set(A,j,i,aij);
			}
		//if(n<7){
			//Memory for matrices AA, X and Y is allocated
			gsl_matrix* AA=gsl_matrix_alloc(n,n);
			gsl_matrix* X =gsl_matrix_alloc(n,n);
			gsl_matrix* Y =gsl_matrix_alloc(n,n);
			
			//The matrix AA is made a copy of matrix A
			gsl_matrix_memcpy(AA,A);
			//The matrix A is printed 
			printf("A random real symmetric matrix A:\n");	
			print_matrix(A);
			
			//The jacobi function is used on A and V (it diagonalizes A since V is the identity matrix)
			jacobi_diag(A,V);
			//The new matrix A is printed 
			printf("Matrix A after jacobi which is now called D (should be diagonal):\n");
			print_matrix(A);
			
			//The matrix X is made. It equals V^T*AA (CblasTrans transposes while CblasNoTranspose don't. They then refere to the values of V and AA). The numbers indicate the values of alpha and beta in the folowing equation which is the ground principle of the blas_dgemm function: X=alpha*(first blas(V))*(second blas(AA))+beta*X
			gsl_blas_dgemm(CblasTrans,CblasNoTrans,1,V,AA,0,X);
			//The matrix Y is made. It equals X*V where X was formed in the equation before
			gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1,X,V,0,Y);
			//These two functions can also be written as V^T*A*V which is what this function does (remember AA is equal to A). The resultin matrix from this is printed. . 
			printf("Matrix V^T*A*V (should equal A after jacobi (D)):\n");
			print_matrix(Y);

			//Now a new matrix X (X=V*A) is made
			gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1,V,A,0,X);
			//Then the new matrix Y is made as Y=X*V^T
			gsl_blas_dgemm(CblasNoTrans,CblasTrans,1,X,V,0,Y);
			
			//The new matrix Y is printed and this should be equal to the diagonal jacobi matrix D since here V*D*V^T is calculated and V*V^T should be equal to the original A since here we change the basis back. 
			printf("Matrix V*D*V^T (should equal original A):\n");
			print_matrix(Y);
	
			//Now V*V^T is calculated as X
			gsl_blas_dgemm(CblasTrans,CblasNoTrans,1,V,V,0,X);
			//This should be equal to the identity matrix and X is therefore printed. 
			printf("Matrix V^T*V (should equal identity matrix (1)):\n");
			print_matrix(X);
		printf("The matrices found in the calculations is the result of the part A. \n");	
	}
	/*
	else{
		int prog=1;
		if(argc>2) prog=atoi(argv[2]);
		fprintf(stderr,"n=%3li, sweeps=",n);
		int sweeps=0;
		switch(prog){
			case 1: sweeps=jacobi(A,V); break;
			case 2: sweeps=jacobi2(A,evec,V); break;
			default: fprintf(stderr,"wrong program");
		}
		fprintf(stderr,"%i\n",sweeps);
		}
	*/
	

