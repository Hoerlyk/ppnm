I have tried to implement this file but i keeps giving me a segmentation fault. 

I suspect that it is my box which doesn't want to compile it. 
I have compared it with Vincent's (we make the homeworks together) 
and it seems equivalent so i suspect that it is caused by my computer. 

I have added the out.txt file manually in order to make the plot work but it 
will disappear if the segmentation fault remains.
Therefore i only give my self about 4 points for it since i suspect it to only 
be a small error and maybe even an error which is local. 

Since Vincent have made the exercise B as well there are too many points in the 
last index but since i only use the first two lines anyway i think that it 
doesn't matter for the plot. 

If it turns out to be a larger error i of cause understand if you subtract 
points for that. 