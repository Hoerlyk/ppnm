#include<math.h>
#include<assert.h>
#include<stdio.h>
#include"adapt.h"
#include<gsl/gsl_integration.h>
#define SQR2 1.41421356237309504880

//This file is recreated so there are no nested functions
int calls;
double f(double x){
	calls++;
	return 1/sqrt(x);
	};

//The different functions - named after which exercise they belongs to

double fA1(double x){calls++; return sqrt(x);}; //former nested function

double fA2(double x){calls++; return 4*sqrt(1-x*x);}; //former nested function



double fB2a(double x){calls++; return 1/sqrt(x);}; //former nested function

double fB2b(double x){calls++; return log(x)/sqrt(x);}; //former nested function

double fB3(double x){calls++; return 4*sqrt(1-x*x);}; //former nested function

double fB4(double x){calls++; return 4*sqrt(1-x*x);}; //former nested function



/*
double f2(double x){calls++; return log(x)/sqrt(x);}; //former nested function

double fp(double x){calls++; return 4*sqrt(1-x*x)/M_PI;}; //former nested function

double fsqrt(double x){calls++; return sqrt(x);}; //former nested function
*/


int main() 
{
//	printf("<head><meta charset='UTF-8'></head><pre>\n");
	printf("\nExercise A\n\n");

	double a=0,b=1,acc=0.001,eps=0.001;
	calls=0;
	double Q=adapt(fA1,a,b,acc,eps);
	double exact=2./3.;
	printf("∫ sqrt(x) from %g to %g : OPEN4\n",a,b);
	printf("              Q = %g\n",Q);
	printf("          exact = %g\n",exact);
	printf("          calls = %d\n",calls);
	printf("estimated error = %g\n",acc+fabs(Q)*eps);
	printf("   actual error = %g\n",fabs(Q-exact));

	printf("It can be seen that the value calculated by the routine (Q) is pretty close to the actual value of the exact integral which is 2/3 \n\n");
	
//	double a=0,b=1,acc=0.001,eps=0.001; //This is commented out since it is equivalent to the conditions in the first integral
	calls=0;
	Q=adapt(fA2,a,b,acc,eps);
	exact=M_PI;
	printf("∫ 4*sqrt(1-x^2) from %g to %g : OPEN4\n",a,b);
	printf("              Q = %g\n",Q);
	printf("          exact = %g\n",exact);
	printf("          calls = %d\n",calls);
	printf("estimated error = %g\n",acc+fabs(Q)*eps);
	printf("   actual error = %g\n",fabs(Q-exact));

	printf("It can be seen that the value calculated by the routine (Q) is pretty close to the actual value of the exact integral which is pi \n");


	printf("\n\nExercise B\n");
	printf("This exercise has three parts\n\n");
	printf("Part 1\n");

	printf("The open quandrature is implemented in the adapt.c file like shown in the example on the homepage.\n\n");


	printf("\nPart 2\n");
	printf("Now the two different methods are compared with the two intergrals mentioned in the exercise \n");

	calls=0;
	Q=adapt(fB2a,a,b,acc,eps);
	exact=2;
	printf("∫ 1/sqrt(x) from %g to %g : OPEN4\n",a,b);
	printf("              Q = %g\n",Q);
	printf("          exact = %g\n",exact);
	printf("          calls = %d\n",calls);
	printf("estimated error = %g\n",acc+fabs(Q)*eps);
	printf("   actual error = %g\n",fabs(Q-exact));
	
	calls=0;
	Q=clenshaw_curtis(fB2a,a,b,acc,eps);
	exact=2;
	printf("\n∫ 1/sqrt(x) from %g to %g : CLENSHAW_CURTIS\n",a,b);
	printf("              Q = %g\n",Q);
	printf("          exact = %g\n",exact);
	printf("          calls = %d\n",calls);
	printf("estimated error = %g\n",acc+fabs(Q)*eps);
	printf("   actual error = %g\n",fabs(Q-exact));

	printf("It can be seen that the Clenshaw curtis method takes a lot fewer calls. \n\n");



	a=0,b=1,acc=0.001,eps=0.001;

	calls=0;
	Q=adapt(fB2b,a,b,acc,eps);
	exact=-4;
	printf("\n∫ ln(x)/sqrt(x) from %g to %g : OPEN4\n",a,b);
	printf("              Q = %g\n",Q);
	printf("          exact = %g\n",exact);
	printf("          calls = %d\n",calls);
	printf("estimated error = %g\n",acc+fabs(Q)*eps);
	printf("   actual error = %g\n",fabs(Q-exact));

	calls=0;
	Q=clenshaw_curtis(fB2b,a,b,acc,eps);
	exact=-4;
	printf("\n∫ ln(x)/sqrt(x) from %g to %g : CLENSHAW_CURTIS\n",a,b);
	printf("              Q = %g\n",Q);
	printf("          exact = %g\n",exact);
	printf("          calls = %d\n",calls);
	printf("estimated error = %g\n",acc+fabs(Q)*eps);
	printf("   actual error = %g\n",fabs(Q-exact));

	printf("It can be seen that the Clenshaw curtis method again takes a lot fewer calls. \n\n");


	printf("\nPart 3\n");

	printf("Now the same intergral as in part A is calculated with the two different methods: \n\n");

	a=0,b=1,acc=1e-9,eps=1e-9;
//	double fp(double x){calls++; return 4*sqrt(1-x*x)/M_PI;}; //nested function

	calls=0; Q=adapt(fB3,a,b,acc,eps);
	exact=M_PI;
	printf("\n∫ 4*sqrt(1-x*2) from %g to %g : OPEN4\n",a,b);
	printf("              Q = %25.20f\n",Q);
	printf("          exact = %25.20f\n",exact);
	printf("          calls =    %d\n",calls);
	printf("estimated error =    %g\n",acc+fabs(Q)*eps);
	printf("   actual error =    %g\n",fabs(Q-exact));

	calls=0; Q=clenshaw_curtis(fB3,a,b,acc,eps);
	exact=M_PI;
	printf("\n∫ 4*sqrt(1-x*x) from %g to %g : CLENSHAW_CURTIS\n",a,b);
	printf("              Q = %25.20f\n",Q);
	printf("          exact = %25.20f\n",exact);
	printf("          calls =    %d\n",calls);
	printf("estimated error =    %g\n",acc+fabs(Q)*eps);
	printf("   actual error =    %g\n",fabs(Q-exact));

	printf("It can be seen that the Clenshaw curtis method again takes a lot fewer calls. Here the accuracy is increased quite a lot so therefore it takes many extra calls to reached the wanted accuracy. \n\n");
	

	printf("\nPart 4:\n");
	printf("Now the integral from part 3 is compared with GSL-routines\n");

	gsl_integration_workspace * w = gsl_integration_workspace_alloc (1000);


	double result, error;
	double expected = M_PI;
			
	gsl_function F;
	F.function =&fB4;
	
	gsl_integration_qags (&F, 0, 1, 0, 1e-9, 1000, w, &result, &error);
	
	printf ("\nIntegration with GSL:\n");
	printf ("result          = %25.20f\n", result);
	printf ("exact result    = %25.20f\n", expected);
	printf ("estimated error = %25.20f\n", error);
	printf ("actual error    = %25.20f\n", result - expected);
	printf ("intervals       =    %zu\n", w->size);

	printf("As seen here the GSL routine is much faster than both of the others.\n\n ");

	gsl_integration_workspace_free (w);


return 0 ;
}
