#include <gsl/gsl_blas.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <math.h>
#include <stdio.h>

//Defines random which is smaller and better so it doesn't take that long time and gives a more random number. 
#define RND ((double)rand()/RAND_MAX)
#define FMT "%7.3f"

//Functions from GS.c
void GS_decomp(gsl_matrix* A, gsl_matrix* R); //Decomposes QR
void QR_back  (gsl_matrix* A, gsl_vector* R); //Performs back substitution
void GS_solve(gsl_matrix* Q, gsl_matrix* R, gsl_vector* b,gsl_vector* qb); 
void GS_inverse(gsl_matrix* Q, gsl_matrix* R, gsl_matrix *B);

//Print matrix function
void printm(gsl_matrix *A)
{
	for(int n=0;n<A->size1;n++)
	{
		for(int m=0;m<A->size2;m++) printf(FMT,gsl_matrix_get(A,n,m));
		printf("\n");
	}
}


int main(int argc, char** argv)
{
	printf("\nIn all exercises there might be a -0.000 this is simply since there might be some small diviations since the routine rounds sometimes. This can be regarded as zero\n\n");

	//Exercise A
	printf("\nExercise A\n");
	printf("Part 1:\n");
	printf("\nQR decomposition:\n");
		
	size_t n=4,m=3; //unsinged int
	if(argc>1)n=atoi(argv[1]);
	if(argc>2)m=atoi(argv[2]);
	if(n<7)
	{
		//Generate random matrix A
		gsl_matrix *A = gsl_matrix_calloc(n,m);
		gsl_matrix *R = gsl_matrix_calloc(m,m);
		for(int i=0;i<n;i++) 
			for(int j=0;j<m;j++) 
				gsl_matrix_set(A,i,j,RND);
		

		//Print the random matrix A
		printf("Some random matrix A:\n"); printm(A);
		
		//Factorization of A into QR
		GS_decomp(A,R);
		
		//Print matrix Q and R
		printf("Matrix Q:\n"); printm(A);
		printf("Matrix R (should be upper triangular):\n"); printm(R);

		
		//Calculate Q^TQ which should be equal to the identity matrix (1 on the diagonal) and print it
		gsl_matrix *qtq = gsl_matrix_calloc(m,m);
		gsl_blas_dgemm(CblasTrans,CblasNoTrans,1.0,A,A,0.0,qtq);
		printf("Matrix Q^TQ (should be the identity matrix):\n"); printm(qtq);
		
		//Calculate and print QR to check that it is equal to A
		gsl_matrix *qr = gsl_matrix_calloc(n,m);
		gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,A,R,0.0,qr);
		printf("Matrix QR (should be equal A):\n"); printm(qr);


		//Exercise A part 2
		printf("\nPart 2:\n");
		printf("\nLinear System: \n");
		
		//Generate a random matrix A which is 4x4 in dimentions
		m=4;
		gsl_matrix *AA = gsl_matrix_calloc(m,m);
		gsl_matrix *QQ = gsl_matrix_alloc(m,m);
		for(int i=0;i<m;i++)
			for(int j=0;j<m;j++)
				gsl_matrix_set(AA,i,j,RND);
		
		//Print the random matrix A. 
		gsl_matrix_memcpy(QQ,AA); //A is copied in order to be able to manipulate with it and still be able to call the original one
		printf("A square random matrix A:\n"); printm(QQ);
		
		//Generate the random vector b and print it
		gsl_vector *b = gsl_vector_alloc(m);
		for(int i=0;i<m;i++)
			gsl_vector_set(b,i,RND);
		printf("A random right-hand side b:\n");
		gsl_vector_fprintf(stdout,b,FMT);
		gsl_matrix *RR = gsl_matrix_calloc(m,m);
		GS_decomp(QQ,RR);
		

		//Print matrix Q and R
		printf("Matrix Q:\n"); printm(QQ);
		printf("Matrix R (should be upper triangular):\n"); printm(RR);

		//Calculate x which is a solution to QRx=b
		gsl_vector *qb = gsl_vector_alloc(m);
		GS_solve(QQ,RR,b,qb);
		printf("Solution x (to problem QRx=b):\n"); 

		//Print Ax
		gsl_vector_fprintf(stdout,qb,FMT);
		gsl_blas_dgemv(CblasNoTrans,1.0,AA,qb,0.0,b);
		printf("Ax, should be equal b:\n"); gsl_vector_fprintf(stdout,b,FMT);
	

		printf("\nExercise B:\n");

		//Generate a random matrix A which is 5x5 in dimentions
		m=3;
		gsl_matrix *AAA = gsl_matrix_calloc(m,m);
		gsl_matrix *QQQ = gsl_matrix_alloc(m,m);
		for(int i=0;i<m;i++)
			for(int j=0;j<m;j++)
				gsl_matrix_set(AAA,i,j,RND);
		
		//Print the random matrix A. 
		gsl_matrix_memcpy(QQQ,AAA); //A is copied in order to be able to manipulate with it and still be able to call the original one
		printf("A square random matrix A:\n"); printm(QQQ);
		
		gsl_matrix *RRR = gsl_matrix_calloc(m,m);
		GS_decomp(QQQ,RRR);
		

		//Print matrix Q and R
		printf("Matrix Q:\n"); printm(QQQ);
		printf("Matrix R (should be upper triangular):\n"); printm(RRR);




		//Calculate the inverse matrix of B (A^-1)
		gsl_matrix *AI = gsl_matrix_calloc(m,m);
		GS_inverse(QQQ,RRR,AI);
		printf("Inverse matrix B=A^{-1}:\n"); printm(AI);
		gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,AAA,AI,0.0,RRR);
		printf("Matrix AB=AA^-1), should be identity matrix:\n"); printm(RRR);
		gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,AI,AAA,0.0,RRR);
		printf("Matrix BA=A^-1A, should be identity matrix:\n"); printm(RRR);
	}
	else{
		printf("n=%li\n",n);
		gsl_matrix* A=gsl_matrix_alloc(n,n);
		gsl_matrix* R=gsl_matrix_alloc(n,n);
		for(int i=0;i<m;i++)for(int j=0;j<m;j++)gsl_matrix_set(A,i,j,RND);
		GS_decomp(A,R);
		gsl_matrix_free(A);
	}
}

