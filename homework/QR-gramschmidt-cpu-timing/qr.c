#include <gsl/gsl_blas.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>

void qrdec(gsl_matrix* A, gsl_matrix* R) {
// QR-decomposition of matrix A, A is replaced with Q, R is filled
int m = A->size2;
for(int i=0;i<m;i++){
	gsl_vector_view ai = gsl_matrix_column(A,i);
	double rii = gsl_blas_dnrm2(&ai.vector); // norm
	gsl_matrix_set(R,i,i,rii);
	gsl_vector_scale(&ai.vector,1/rii); //normalization
	for(int j=i+1;j<m;j++){
		gsl_vector_view aj = gsl_matrix_column(A,j);
		double rij;
		gsl_blas_ddot(&ai.vector,&aj.vector,&rij);
		gsl_blas_daxpy(-rij,&ai.vector,&aj.vector); // -rij*ai+aj -> aj
		gsl_matrix_set(R,i,j,rij);
		gsl_matrix_set(R,j,i,0);
		}
	}
}

void qrbak(gsl_matrix *R, gsl_vector *x) {
int m=R->size1;
for(int i=m-1;i>=0;i--){
	double s=gsl_vector_get(x,i);
	for(int k=i+1;k<m;k++)
		s-=gsl_matrix_get(R,i,k)*gsl_vector_get(x,k);
	gsl_vector_set(x,i,s/gsl_matrix_get(R,i,i));
	}
}

void qrsolve(gsl_matrix *Q, gsl_matrix *R, gsl_vector *b, gsl_vector *x) {
gsl_blas_dgemv(CblasTrans,1.0,Q,b,0.0,x); // x = 1*Q^T*b + 0*x
qrbak(R,x); // backsustitution
}

void qrinv(gsl_matrix *Q, gsl_matrix* R, gsl_matrix *B) {
int n=R->size1;
gsl_vector *b = gsl_vector_calloc(n);
gsl_vector *x = gsl_vector_calloc(n);
for(int i=0;i<n;i++){
	gsl_vector_set(b,i,1.0);
	qrsolve(Q,R,b,x);
	gsl_vector_set(b,i,0.0);
	gsl_matrix_set_col(B,i,x);
	}
gsl_vector_free(b);
gsl_vector_free(x);
}
