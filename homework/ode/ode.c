#include<math.h>
#include<stdio.h>

void rkstep12(	
		int n,
	void f(int n,double t,double y[],double dydt[]),	/*dy/dt=f(t,y) */
	double t,	/* Current value of variable */
	double y[], 	/* Current y-value of the function */
	double h,	/* The step to be taken */ 
	double yh[], 	/* The output after step h - y(t+h) */
	double dy[]	/* The error estimate */
										)
{
	double k0[n]; 
	f(n,t,y,k0);
	double y1[n]; 
	for(int i=0; i<n; i++) 
		y1[i]=y[i]+(0.5 *h)*k0[i];
	double k1[n]; 
	f(n,t+0.5*h,y1,k1);
	for(int i=0; i<n; i++) 
		yh[i]=y[i]+h*k1[i];
	for(int i=0; i<n; i++) 
		dy[i]=(k1[i]-k0[i])*h;
}

void driver(
		int  n,						/* Define n for y[n] */
		void f(int n,double t,double*y,double*dydt), 	/* dy/dt=f(t,y) */
		double a,              				/* The start-point a */
		double*y,              				/* y(a) -> y(b) */
		double b,              				/* The end-point of the integration */
		double h,                    			/* Initial step-size */
		double acc,            				/* Absolute accuracy goal */
		double eps             				/* Relative accuracy goal */
	   )
{
	double t=a;
	printf("%9.3g ", t);
	for(int i=0; i<n; i++)
		printf("%9.3g ", y[i]);
	printf("\n");
	while(t<b)
	{
		if(t+h>b)h=b-t;
		double yh[n],dy[n];
		rkstep12(n,f,t,y,h,yh,dy);
		double sum=0; 
		for(int i=0; i<n; i++)
			sum+=y[i]*y[i];
		double norm_y=sqrt(sum);
		sum=0;
		for(int i=0; i<n; i++)
			sum+=dy[i]*dy[i];
		double err=sqrt(sum);
		double tol=(acc+eps*norm_y)*sqrt(h/(b-a));
		if(err<tol)
		{
			t=t+h;
			for(int i=0; i<n; i++) 
				y[i]=yh[i];	
			printf("%9.3g ", t);
			for(int i=0; i<n; i++)
				printf("%9.3g ", y[i]);
			printf("\n");
		}
		if(err>0) h*=0.95*pow(tol/err,0.25);
		else h*=2;
	}
}

