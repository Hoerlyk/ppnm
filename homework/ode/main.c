#include<stdio.h>
#include<math.h>
#include"ode.h"
#include<gsl/gsl_vector.h>

void f(int n,double t,double*y,double*dydt)
{
	dydt[0]=+y[1];
	dydt[1]=-y[0];
}

void O(int n,double x,double*y,double*dydt)
{
	dydt[0]=+y[1];
	dydt[1]=2*(x*x/2-0.5)*y[0];
}

void SIR(int n,double x,double*y,double*dydt)
{
	double N=5000000,Tr=15,Tc=1;
	dydt[0]=-y[1]*y[0]/(N*Tc);
	dydt[1]=y[0]*y[1]/(N*Tc)-y[1]/Tr;
	dydt[2]=y[1]/Tr;
}

int main(int argc, char** argv)
{
/*	printf("\nExercise A\n");*/

	double a=0,b=2*M_PI,h=0.1,acc=1e-2,eps=1e-2;
	int n=2;
	double y[n];
	y[0]=0;
	y[1]=1;
	printf("#index 0 u''=-u\n");
	driver(n,f,a,y,b,h,acc,eps);

/*	printf("In exercise A part 3 the function sin(x) is used since it fits with the description that u''=-u.\n ");
	printf("The results can be seen in the figure sin.png\n");
*/	


	a=0; b=100; h=0.01; acc=1e-3; eps=1e-3;
	n=3;
	y[0]=4500000;
	y[1]=50000;
	y[2]=0;
	printf("\n\n #index 1 SIR\n");
	driver(n,SIR,a,y,b,h,acc,eps);
	
/*	printf("In exercise A part 4 the function SIR-model is used like described in the exercise\n ");
	printf("The results can be seen in the figure SIR.png\n");
*/

return 0;
}

