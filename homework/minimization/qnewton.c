#include<stdio.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>

//The static constant delta is defined as the squareroot of the double of the machine epsilon
static const double DELTA=sqrt(2.22045e-16);

//This function calculates the gradient at a given point in order to see when a local minima or maxima is reached. 
void numeric_gradient(double F(gsl_vector*), gsl_vector*x, gsl_vector*grad){
	double fx=F(x);
	for(int i=0;i<x->size;i++){
		double dx,xi=gsl_vector_get(x,i);
		if(fabs(xi)<sqrt(DELTA)) dx=DELTA; //xi must be smaller than the static constant DELTA -
		else dx=fabs(xi)*DELTA;
		gsl_vector_set(x,i,xi+dx); 
		gsl_vector_set(grad,i,(F(x)-fx)/dx); //This is the gradient vector which is returned by the function
		gsl_vector_set(x,i,xi);
	}
}

int qnewton(double F(gsl_vector*), gsl_vector*x, double acc) {
	int n=x->size,nsteps=0,nbad=0,ngood=0;
	//The matrices needed is allocated
	gsl_matrix* B=gsl_matrix_alloc(n,n);
	gsl_vector* gradient=gsl_vector_alloc(n);
	gsl_vector* Dx=gsl_vector_alloc(n);
	gsl_vector* z=gsl_vector_alloc(n);
	gsl_vector* gz=gsl_vector_alloc(n);
	gsl_vector* y=gsl_vector_alloc(n);
	gsl_vector* u=gsl_vector_alloc(n);
	gsl_vector* a=gsl_vector_alloc(n);
	gsl_matrix_set_identity(B); //The B matrix starts as an identity matrix (B is the same as H^-1)
	numeric_gradient(F,x,gradient);
	double fx=F(x),fz;
	while(nsteps<3000){
		nsteps++;
		gsl_blas_dgemv(CblasNoTrans,-1,B,gradient,0,Dx); //Equals equation 6 in the book
		if(gsl_blas_dnrm2(Dx)<DELTA*gsl_blas_dnrm2(x)) //It is checked weather the step is smaller than the chosen precision (DELTA)
			{fprintf(stderr,"qnewton: |Dx|<DELTA*|x|\n"); break;}
		if(gsl_blas_dnrm2(gradient)<acc)
			{fprintf(stderr,"qnewton: |grad|<acc\n"); break;}
		
		double lambda=1;
		while(1){ //In this function the size of the step is determined
			gsl_vector_memcpy(z,x);
			gsl_vector_add(z,Dx);
			fz=F(z);
			double sTg; gsl_blas_ddot(Dx,gradient,&sTg);
			if(fz<fx+0.01*sTg){ ngood++; break; }
			if(lambda<DELTA){
				nbad++;
				gsl_matrix_set_identity(B);
				break;
				}
			lambda*=0.5;
			gsl_vector_scale(Dx,0.5);
		}
		numeric_gradient(F,z,gz); //The gradient function is called
		gsl_vector_memcpy(y,gz);
		gsl_blas_daxpy(-1,gradient,y); // y=grad(x+step)-grad(x) 
		gsl_vector_memcpy(u,Dx); // u=step
		gsl_blas_dgemv(CblasNoTrans,-1,B,y,1,u); // u=step-By 

		double sTy,uTy; //T means transposed
		gsl_blas_ddot(Dx,y,&sTy);
		if(fabs(sTy)>1e-12){ // symmetric Broyden's update - only used if the step is larger than 1e-12
			gsl_blas_ddot(u,y,&uTy);
			double gamma=uTy/2/sTy;
			gsl_blas_daxpy(-gamma,Dx,u); // u=u-gamma*s
			gsl_blas_dger(1/sTy,u,Dx,B); // B= B + u*s^T/s^T*y
			gsl_blas_dger(1/sTy,Dx,u,B); // B= B + s*u^T/s^T*y
		}
		gsl_vector_memcpy(x,z);
		gsl_vector_memcpy(gradient,gz);
		fx=fz;
	}
//The allocated matrices are freed again
gsl_matrix_free(B);
gsl_vector_free(gradient);
gsl_vector_free(Dx);
gsl_vector_free(z);
gsl_vector_free(gz);
gsl_vector_free(y);
gsl_vector_free(u);
gsl_vector_free(a);
//The final values of the function is returned together with the number of steps
fprintf(stderr,"qnewton: nsteps=%i ngood=%i nbad=%i fx=%.1e\n"
		,nsteps,ngood,nbad,fx);
return nsteps;
}
