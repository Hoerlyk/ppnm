#include<stdio.h>
#include<math.h>
#include<float.h>
#define FOR(i) for(int i=0;i<dim;i++)
#define SET_IDENTITY(B) FOR(i)FOR(j)B[i][j]=0;FOR(i)B[i][i]=1;
static const double DELTA=sqrt(DBL_EPSILON);

void numeric_gradient
(int dim,double F(int dim,double* x),double*x,double*grad){
	double fx=F(dim,x);
	FOR(i){
		double dx=DELTA,xi=x[i];
		if(fabs(xi)>sqrt(DELTA)) dx=fabs(xi)*DELTA;
		x[i]+=dx;
		grad[i]=(F(dim,x)-fx)/dx;
		x[i]=xi;
	}
}

int broyden(int dim,double F(int,double*), double*x, double acc) {
	int nsteps=0;
	double B[dim][dim];
	SET_IDENTITY(B);
	double gx[dim];
	numeric_gradient(dim,F,x,gx);
	double fx=F(dim,x),fz;
	while(nsteps<1000){
		double normgrad=0;
		FOR(i)normgrad+=gx[i]*gx[i];
		normgrad=sqrt(normgrad);
		if(normgrad<acc) break;
		nsteps++;
		double step[dim],z[dim];
		FOR(i){ step[i]=0; FOR(j) step[i]-=B[i][j]*gx[j]; }
		double lambda=1;
		while(1){
			FOR(i)z[i]=x[i]+step[i];
			fz=F(dim,z);
			double sTg=0; FOR(i)sTg+=step[i]*gx[i];
			if(fz<fx+0.01*sTg) break;
			if(lambda<DELTA){ SET_IDENTITY(B); break; }
			lambda/=2;
			FOR(i)step[i]/=2;
		}
		double gz[dim];
		numeric_gradient(dim,F,z,gz);
		double y[dim];
		FOR(i)y[i]=gz[i]-gx[i];
		double u[dim];
		FOR(i){ u[i]=step[i]; FOR(j) u[i] -= B[i][j]*y[j]; }
		double sTy=0; FOR(i)sTy += step[i]*y[i];
		if(fabs(sTy)>DELTA){
/*
			FOR(i)FOR(j)B[i][j]+=u[i]*step[j]/sTy; // Broyden update
*/
			double uTy=0; FOR(i) uTy += u[i]*y[i];
			double gamma=uTy/2/sTy; // symmetric Broyden update
			double a[dim]; FOR(i) a[i] = u[i]-gamma*step[i];
			FOR(i)FOR(j)B[i][j] += a[i]*step[j]/sTy;
			FOR(i)FOR(j)B[i][j] += step[i]*a[j]/sTy;
		}
		FOR(i)x[i]=z[i];
		FOR(i)gx[i]=gz[i];
		fx=fz;
	}
return nsteps;
}
