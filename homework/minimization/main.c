#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<math.h>
#include<stdio.h>
#include<stdlib.h>

//The qnewton function is called - it is implemented like it was in the homework about roots
int qnewton(double cost(gsl_vector*x), gsl_vector*x, double acc);

//
void numeric_gradient(double cost(gsl_vector*x),gsl_vector*x, gsl_vector*g);

//The vector print function is made available for later use - it is made like it was in the gsl-matrix lecture
void vector_print(const char* s,gsl_vector* v){
	printf("%s",s);
	for(int i=0;i<v->size;i++) printf("%12g ",gsl_vector_get(v,i));
	printf("\n");
	}

//The Rosenbock's valley function is created like in the example on the homepage
//the function has the form f(x,y)=(1-x)^2+100*(y-x^2)^2
double rosen(gsl_vector*v){
	double x=gsl_vector_get(v,0);
	double y=gsl_vector_get(v,1);
	return pow(1-x,2)+100*pow(y-x*x,2);
}

//The Himmelblau's function is created like in the example on the homepage
//The function has the form f(x,y)=(x^2+y-11)^2+(x+y^2-7)^2
double himmel(gsl_vector*v){
	double x=gsl_vector_get(v,0);
	double y=gsl_vector_get(v,1);
	return pow(x*x+y-11,2)+pow(x+y*y-7,2);
}

//The Breit-Wigner function is created like in the part B on the homepage
//The function has the form A/((E-m)^2+Gamma^2/4)
double Breit_Wigner(double E, gsl_vector*v){
	double m=gsl_vector_get(v,0);
	double Gamma=gsl_vector_get(v,1);
	double A=gsl_vector_get(v,2);
	return A/(pow(E-m,2)+pow(Gamma,2)/4);
}

double diviation(gsl_vector*v){
	int ndata=30;
	double E[ndata], sigma[ndata], dsigma[ndata];

	//data.txt now contains the values from the exercise
	FILE *data = fopen("Higgs_data.txt","r"); 	//Reads the file Higgs_data.txt

	if(data == NULL )//|| out_data_test == NULL) 
		{
		printf("No data in file\n");
		exit(-1);
		}
	else{
		for(int i=0; i<ndata; i++){
			fscanf(data,"%lf %lf %lf",&E[i], &sigma[i], &dsigma[i]);
			}
	}
	fclose(data);

	double result=0;
	for(int i=0; i<ndata; i++){
		double X=pow((Breit_Wigner(E[i],v)-sigma[i]),2)/pow(dsigma[i],2); //This equation is given in the exercise
		result += X;
		}
return result;
}

int main(){
printf("Part A\n\n");

	int n=2,nsteps;
	gsl_vector* x=gsl_vector_alloc(n);
	gsl_vector* g=gsl_vector_alloc(n);
	double acc=1.00000e-3; //The accuracy



	//Now the minimization for the Rosenbrocks function is made by calling the qnewton and the numeric_gradient functions and then just extracting data. 
	printf("\nMINIMIZATION OF THE ROSENBROCK'S VALLEY FUNCTION\n");
	printf("It has the form f(x,y)=(1-x)^2+100(y-x^2)^2 in this example\n\n");
	gsl_vector_set(x,0,20);
	gsl_vector_set(x,1,20);
	vector_print("Chosen start point (x,y)                 :",x);
	printf      ("Start value of the function              : %g\n",rosen(x));
	nsteps=qnewton(rosen,x,acc);
	
	printf("\nThe minimum of this function should be at (x,y)=(a,a^2) where a is 1 in this case. This means that the minimum should be at (1,1)\n\n");
	vector_print("Minimum found at point (x,y)             :",x);
	printf      ("Number of steps used to reach minimum    : %i\n",nsteps);
	printf      ("Value of the function at minimum         : %g\n",rosen(x));
	numeric_gradient(rosen,x,g);
	printf      ("Chosen accuracy                          : %.1e\n",acc);
	vector_print("Gradient at the min (must be very small) :",g);

	printf	    ("\nThe minimum found is indeed very close to (1,1) and it was found very fast considering (in only %i steps) that we started at (20,20). Futher more the gradient is very small at this point which indicates that it is indeed a minimum since a gradient is zero in a minimum or maximum. \n",nsteps);
	
	//Now the minimization for the Himmelblau's function is made by calling the qnewton and the numeric_gradient functions and then just extracting data. 
	printf("\n\n\nMINIMIZATION OF THE HIMMELBLAU'S FUNCTION\n");
	printf("It has the form f(x,y)=(x^2-y-11)^2+(x-y^2-7)^2 in this example\n\n");
	gsl_vector_set(x,0,20);
	gsl_vector_set(x,1,20);
	vector_print("Chosen start point (x,y)                 :",x);
	printf      ("Start value of the function              : %g\n",himmel(x));
	nsteps=qnewton(himmel,x,acc);
	
	printf("\nThe minimum of this function should be at either (x,y)=(3,2), (x,y)=(-2.805,3.1313), (x,y)=(-3.779,-3.283) or (x,y)=(3.584,-1.848). (There should be four identical local minima).\n\n");
	vector_print("Minimum found at point (x,y)             :",x);
	printf      ("Number of steps used to reach minimum    : %i\n",nsteps);
	printf      ("Value of the function at minimum         : %g\n",himmel(x));
	numeric_gradient(himmel,x,g);
	printf      ("Chosen accuracy		               : %.1e\n",acc);
	vector_print("Gradient at the min (must be very small) :",g);

	printf	    ("\nThe minimum found is at (x,y)= (-3.78,-3.28) which must be the third of the mentioned minima. It was relatively fast (in only %i steps) considering the complexity of the function and that we started at (20,20). Futher more the gradient is very small at this point, which indicates that it is indeed a minimum since a gradient is zero in a minimum or maximum. \n\n",nsteps);



printf("\nPart B\n");
	int p=3,psteps;
	gsl_vector* y=gsl_vector_alloc(p);
	gsl_vector* f=gsl_vector_alloc(p);
	double accu=1.00000e-8; //The accuracy

	//Now the minimization for the Breit-Wigner function is made by calling the qnewton to the diviation function (as made above) and the numeric_gradient functions and then just extracting data. 
	printf("\nMINIMIZATION OF THE BREIT-WIGNER FUNCTION TO DATA FROM THE HIGGS-BOSON\n");
	printf("It has the form f(E|m,Gamma,A)=A/((E-m)^2+Gamma^2/4) in this example\n\n");
	gsl_vector_set(y,0,122);
	gsl_vector_set(y,1,0.005);
	gsl_vector_set(y,2,0.01);
	vector_print("Chosen start point (m,Gamma,A)           :",y);
	printf      ("Start value of the function              : %g\n",diviation(y));
	psteps=qnewton(diviation,y,accu);
	
	printf("\nThe minimum of this function should be where m=125.3. Since in the exercise it is stated that the mass of the Higgs-boson should be 125.3(6) GeV/c^2. A is a constant of proportionality so therefore its value doesn't matter that much for the understanding of the fit. Lastly the Gamma is associated with the width. \n\n");
	vector_print("Minimum found at point (m,Gamma,A)       :",y);
	printf      ("Number of steps used to reach minimum    : %i\n",psteps);
	printf      ("Value of the function at minimum         : %g\n",diviation(y));
	numeric_gradient(diviation,y,f);
	printf      ("Chosen accuracy                          : %.1e\n",accu);
	vector_print("Gradient at the min (must be very small) :",f);

	printf	    ("\nThe minimum is found at (125.97,2.086,9.876). This means that the mass is estimated to be 125.97 GeV/c^2 which is within the uncertainty of the CERN-estimated value. The A-value is simply a constant of proportionality which seems to be related to the mass and decay width (Gamma) of the particle and therefore the result only makes sence if it is positive - which it is here. Finaly the width (Gamma) is found to be 9.976. The width (also called decay width) and the mean lifetime of a partile is related as tau=hbar/Gamma but since the main equation isn't in the same as the relativistic Breit-Wigner distribution it is difficult to intercept this result into something meaningfull. The mean lifetime should however be about 1.56*10^-22 s. The result for this fitting was found in %i steps. \n",psteps);
	
	


return 0;
}
