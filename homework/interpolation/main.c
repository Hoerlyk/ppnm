#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<assert.h>
#include"functions.h"

//double linterp(int, double*, double*, double);
//double linterp_integ(int, double*,double*, double);


int main(){
	//Datagenerator
	int n=16;
	double x[n],y[n];
//	double a=-2, b=2;

	printf("#index 0: data from function 1/(1+x^2) (x,y) \n");
	for(int i=0;i<n;i++)
		{
		x[i]=(double) i/2.5;
//		x[i]=(double) i/(n-1);
		y[i]=1/(1+pow(x[i],2));
		printf("%g %g\n",x[i],y[i]);
		}
	
	printf("\n\n");	
	//A linspace is made for the later integration and then the data is saved in index 0 together with the original x and y-values
//	int i=0;
//	for double z=x[0]; z<=[n-1];z+=1./16)
		
	int N=200; double z[N];
	for(int i=0;i<N;i++)
		{
		z[i]= (10+i)/33;
//	return z[i];
		}

	//Linear interpolation data
	double f_x[N], F_x[N];
	printf("#index 1: Data from the linear spline - integrated (x f(x) F(x))\n");
	for(int i=0;i<N;i++)
		{
		f_x[i]=linterp(n,x,y,z[i]);
		F_x[i]=linterp_integ(n,x,y,z[i]);
		printf("%10g %10g %10g \n",z[i],f_x[i],F_x[i]);
		}
	
	printf("\n \n");



/*	qspline* Q = qspline_alloc(n,x,y);
	cubic_spline* C = cubic_spline_alloc(n,x,y);
	double dz=0.1;
	for(double z=x[0];z<=x[n-1];z+=dz){
		double lz=linterp(n,x,y,z);
		double qz=qspline_eval(Q,z);
		double cz=cubic_spline_eval(C,z);
		printf("%g %g %g %g\n",z,lz,qz,cz);
	}
	qspline_free(Q);
	cubic_spline_free(C);

	double a=-9.9,b=9.9;
	n/=2;
	fprintf(stderr,"#m=0,S=4\n");
	for(int i=0;i<n;i++){
		x[i]=a+(b-a)*i/(n-1);
		y[i]=x[i]*x[i]*x[i];
		fprintf(stderr,"%g %g\n",x[i],y[i]);
	}
	Q = qspline_alloc(n,x,y);
	C = cubic_spline_alloc(n,x,y);
	fprintf(stderr,"#m=1,S=0\n");
	for(double z=x[0];z<=x[n-1];z+=dz) fprintf(stderr,"%g %g\n",z,linterp(n,x,y,z));
	fprintf(stderr,"#m=2,S=0\n");
	for(double z=x[0];z<=x[n-1];z+=dz) fprintf(stderr,"%g %g\n",z,qspline_eval(Q,z));
	fprintf(stderr,"#m=3,S=0\n");
	for(double z=x[0];z<=x[n-1];z+=dz) fprintf(stderr,"%g %g\n",z,cubic_spline_eval(C,z));
	qspline_free(Q);
	cubic_spline_free(C);
*/
return 0;
}
