#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<math.h>

int binsearch(int n, double* x, double z){
	assert(n>1 && x[0]<=z && z<=x[n-1]); 
	int i=0, j=n-1;
	while(j-i>1){
		int mid=(i+j)/2;
		if(z>x[mid]) 
			i=mid; 
		else j=mid;
	}
return i;
}

	// linear spline

double linterp(int n, double x[], double y[], double z){
	int i=binsearch(n,x,z);
		
	//In a linear interpolation the line is called f(x)=ax+b here f(x) is called f_z since z is x-values after spline is implemented. 
	//Here ax is found for each i as dy/dx 
	double dy=y[i+1]-y[i];
	double dx=x[i+1]-x[i]; 
		assert(dx>0);
	
	double bi=y[i];
	double ai=dy/dx;
	double f_x=bi+ai*(z-x[i]);
return f_x;
}

	// Integral of linear spline
	//The integral of a linear function f(x)=b+ax is F(x)=b*x+a*x^2/2+c here c is regarded as zero. 
double linterp_integ(int n, double x[], double y[], double z){
	int i=binsearch(n,x,z);

	double offset=0;

	for(int o=0;o<i;o++)
		{
		double dy=y[o+1]-y[o];
		double dx=x[o+1]-x[o]; 
			assert(dx>0);
	
		double bo=y[o];
		double ao=dy/dx;
		
		offset += bo*dx+ao*pow(dx,2)/2;
		}

	double dy=y[i+1]-y[i];
	double dx=x[i+1]-x[i];
		assert(dx>0); 
	double ai=dy/dx;
	double bi=y[i];
	
	//If f(x)=ax+b then F(x)=ax^2/2+b*x+c - here c=0
	double F_integ=bi*(z-x[i])+ai*pow((z-x[i]),2)/2;

	double F_x=offset+=F_integ;
return F_x;
}




